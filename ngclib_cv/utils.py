"""Utils functions"""
import numpy as np
from media_processing_lib.image import to_image
from ngclib.models import NGCNode
from ngclib.logger import logger

# pylint: disable=unused-argument
def default_transform(x: np.ndarray) -> np.ndarray:
    """Default transform just converts to float and adds a final dim if not provided. Output shape: (H, W, C)."""
    x = np.float32(x)
    if x.shape[0] == 1:  # pylint: disable=unsubscriptable-object
        x = x[0]  # pylint: disable=unsubscriptable-object
    if len(x.shape) == 2:
        x = np.expand_dims(x, axis=-1)
    return x


# pylint: disable=unused-argument
def default_plot_fn(x: np.ndarray, node: NGCNode) -> np.ndarray:
    """Convert anything else to RGB image"""
    logger.warning(f"Default plot fn called for node '{node.name}' (type: {node}). Overwrite this!")
    x = np.clip(x, 0, 1)
    x = to_image(x)
    return x


# pylint: disable=unused-argument
def black_image_plot_fn(x: np.ndarray, node: NGCNode) -> np.ndarray:
    """Null node to black RGB image. Used in collages."""
    assert len(x.shape) == 3
    return np.zeros((x.shape[0], x.shape[1], 3), dtype=np.uint8)
