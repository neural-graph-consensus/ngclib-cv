"""Augmentation functions builder"""
from typing import Callable, Dict, List

from .random_zoom import RandomZoom

def get_augmentation(name: str, **kwargs) -> Callable:
    """Main function used to build ngclib-cv augmentations"""
    if name == "random_zoom":
        return RandomZoom(**kwargs)

    assert False, f"Unknown augmentation: '{name}'"

def augmentation_from_train_cfg(augmentation_dict: Dict) -> List[Callable]:
    """Given a train config file, get the augmentation list. TODO: node-specific."""
    general_augmentation = []
    if "general" in augmentation_dict:
        for name, augm_args in augmentation_dict["general"].items():
            general_augmentation.append(get_augmentation(name, **augm_args))
    return general_augmentation
