"""Soft edges node definition"""
from .rgb import RGB


class SoftEdges(RGB):
    """Soft edges node implementation"""
