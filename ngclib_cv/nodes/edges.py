"""Edges node definition"""
from overrides import overrides
import numpy as np
from .semantic import Semantic
from ..utils import default_plot_fn


class Edges(Semantic):
    """Edges node class implementation"""

    def __init__(self, name: str):
        super().__init__(name, semantic_classes=2, semantic_colors=[[0, 0, 0], [255, 255, 255]], encoding="onehot")

    @overrides
    def plot_fn(self, x: np.ndarray) -> np.ndarray:
        return default_plot_fn(x, self)
