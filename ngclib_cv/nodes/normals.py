"""
Normal node definition
TODOs:
 - representation type: unit vector, 3 angles, cos/sin etc.
 - metrics to convert to [0 - 180] l1 angles differences
"""
from overrides import overrides
import numpy as np
from ngclib.models import NGCNode
from ..utils import default_transform, default_plot_fn


class Normals(NGCNode):
    """Normals node implementation"""

    _num_dims = {"euler": 3, "quaternion": 4}

    def __init__(self, name: str, representation: str):
        super().__init__(name, num_dims=Normals._num_dims[representation])
        self.representation = representation

    @overrides
    def load_from_disk(self, x: np.ndarray) -> np.ndarray:
        return default_transform(x)

    @overrides
    def save_to_disk(self, x: np.ndarray) -> np.ndarray:
        """Converts back normals to save on disk as float16"""
        return default_transform(x).astype(np.float16)

    @overrides
    def plot_fn(self, x: np.ndarray) -> np.ndarray:
        return default_plot_fn(x, self)
