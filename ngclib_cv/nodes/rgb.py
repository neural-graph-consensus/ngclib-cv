"""RGB node definition"""
from overrides import overrides
from ngclib.models import NGCNode
from media_processing_lib.image import to_image
import numpy as np

from ..utils import default_transform


class RGB(NGCNode):
    """RGB node implementation"""

    num_dims = 3

    def __init__(self, name: str):
        super().__init__(name, num_dims=3)

    @overrides
    def load_from_disk(self, x: np.ndarray) -> np.ndarray:
        return default_transform(x)

    @overrides
    def save_to_disk(self, x: np.ndarray) -> np.ndarray:
        return default_transform(x).astype(np.float16)

    @overrides
    def plot_fn(self, x: np.ndarray) -> np.ndarray:
        return to_image(np.clip(x, 0, 1))
