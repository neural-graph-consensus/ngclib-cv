"""Soft segmentation node definition. Basically a RGB with colors clustered by some soft segmentation"""
from .rgb import RGB


class SoftSegmentation(RGB):
    """Soft segmentation node implementation"""
