"""Init file"""
from .rgb import RGB
from .hsv import HSV
from .depth import Depth
from .semantic import Semantic
from .edges import Edges
from .normals import Normals
from .soft_segmentation import SoftSegmentation
