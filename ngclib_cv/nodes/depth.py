"""Depth node definition"""
from ngclib.models import NGCNode
from overrides import overrides
from media_processing_lib.image import to_image
from matplotlib.cm import get_cmap
import numpy as np
from ..utils import default_transform


class Depth(NGCNode):
    """Depth node implementation"""

    num_dims = 1

    def __init__(self, name: str, max_depth_meters: float):
        super().__init__(name, num_dims=1)
        self.max_depth_meters = max_depth_meters

    @overrides
    def load_from_disk(self, x: np.ndarray) -> np.ndarray:
        """Converts from disk [0:max_meters] to [0:1] and clips it properly"""
        x = x / self.max_depth_meters
        x = np.clip(x, 0, 1).astype(np.float32)
        return default_transform(x)

    @overrides
    def save_to_disk(self, x: np.ndarray) -> np.ndarray:
        """Converts back to [0: max_depth_meters] and float16"""
        y = np.clip(x, 0, 1)
        y = (y * self.max_depth_meters).astype(np.float16)
        return y

    @overrides
    def plot_fn(self, x: np.ndarray) -> np.ndarray:
        if x.max() > 1:
            x = x / self.max_depth_meters
        y = np.clip(x, 0, 1).squeeze()
        cmap = get_cmap("plasma")
        y = cmap(y)[..., 0:3]
        y = to_image(y)
        return y
