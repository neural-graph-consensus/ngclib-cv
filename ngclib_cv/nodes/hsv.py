"""HSV node definition"""
from .rgb import RGB


class HSV(RGB):
    """HSV node implementation"""
