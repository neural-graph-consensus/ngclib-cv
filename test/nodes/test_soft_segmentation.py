from ngclib_cv.nodes import SoftSegmentation


def test_node_hsv_1():
    node = SoftSegmentation("soft_segmentation")
    assert not node is None
    assert node.name == "soft_segmentation"
    assert node.num_dims == 3
