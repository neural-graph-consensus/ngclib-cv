from ngclib_cv.nodes import Depth

# from nwgraph.node import build_dummy_identity_node


def test_node_depth_1():
    node = Depth("depth", max_depth_meters=300)
    assert not node is None
    assert node.name == "depth"
    assert node.max_depth_meters == 300
    assert node.num_dims == 1


if __name__ == "__main__":
    test_node_depth_1()
