from ngclib_cv.nodes import Edges


def test_node_edges_1():
    node = Edges("edges")
    assert not node is None
    assert node.name == "edges"
    assert node.num_dims == 2
