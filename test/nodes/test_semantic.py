import numpy as np
from ngclib_cv.nodes import Semantic


def test_node_semantic_1():
    semantic_classes = 12
    semantic_colors = np.arange(semantic_classes * 3).reshape(semantic_classes, 3)
    node = Semantic(
        name="semantic", semantic_classes=semantic_classes, semantic_colors=semantic_colors, encoding="onehot"
    )
    assert not node is None
    assert node.name == "semantic"
    assert np.allclose(node.semantic_colors, semantic_colors)
    assert node.semantic_classes == list(range(12))
    assert node.num_classes == 12
    assert node.encoding == "onehot"


def test_node_semantic_2():
    semantic_classes = ["class1", "class2", "class3"]
    semantic_colors = [[0, 0, 0], [1, 1, 1], [2, 2, 2]]
    node = Semantic(
        name="semantic", semantic_classes=semantic_classes, semantic_colors=semantic_colors, encoding="soft"
    )
    assert not node is None
    assert node.name == "semantic"
    assert np.allclose(node.semantic_colors, semantic_colors)
    assert node.semantic_classes == semantic_classes
    assert node.num_classes == 3
    assert node.encoding == "soft"
