from ngclib_cv.nodes import HSV


def test_node_hsv_1():
    node = HSV("hsv")
    assert not node is None
    assert node.name == "hsv"
    assert node.num_dims == 3
