from ngclib_cv.nodes import RGB


def test_node_rgb_1():
    node = RGB("rgb")
    assert not node is None
    assert node.name == "rgb"
    assert node.num_dims == 3
