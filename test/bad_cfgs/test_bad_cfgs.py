from typing import Dict, List
import yaml
from nwgraph import Node
from ngclib.graph_cfg import GraphCfg
from pathlib import Path
from ngclib.models import build_model
from ngclib_cv.nodes import *

cwd = Path(__file__).absolute().parent


def get_nodes(cfg: Dict) -> List[Node]:
    """Basic implementation of getNodes, creating a subtype for all required types by adding a linear projection
    for the encoder and identity for decoder.
    """
    nodes = []
    for i in range(len(cfg["nodeNames"])):
        node_name = cfg["nodeNames"][i]
        node_params = {} if not node_name in cfg["hyperParameters"] else cfg["hyperParameters"][node_name]
        node_params["name"] = node_name
        node_type = cfg["nodeTypes"][i]
        node_type = globals()[node_type]
        node = node_type(**node_params)
        nodes.append(node)
    return nodes


class TestBadCfgs:
    def test_bad_cfg_1(self):

        graph_cfg = GraphCfg(yaml.safe_load(open(cwd / "graph_ngcv1_sl_no_input_node.yaml", "r")))

        try:
            _ = build_model(get_nodes(graph_cfg), graph_cfg)
            assert False
        except:
            pass


if __name__ == "__main__":
    TestBadCfgs().test_bad_cfg_1()
