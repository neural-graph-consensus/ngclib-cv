## **NGCLib Computer vision**

The idea is that under ngclib we have generic primitives (NGC graph, ngcdir, iteration prologue, edge/graph types etc.), while here we hold all the computer vision related primitives (RGB node, Semantic node, etc.)

Stubs for CV related nodes (RGB, Halftone, Semantic, etc.) are under `ngclib_cv/nodes/` submodule.
