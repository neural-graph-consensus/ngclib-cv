import yaml
import shutil
import tempfile
from typing import Dict, List
from pathlib import Path
from ngclib.utils import generate_random_data, NGCDir
from ngclib.trainer import NGCTrainer
from ngclib.models import build_model
from ngclib_cv.nodes import *
from nwgraph import Node
from nwgraph.node import build_dummy_linear_node
from lightning_module_enhanced import LightningModuleEnhanced

tmpDirName = None

graphV1CfgStr = """
NGC-Architecture: NGC-V1
nodeTypes: [RGB, Semantic, Depth]
nodeNames: [rgb, semantic, depth]
inputNodes: [rgb]
edges: [ # Edges are defined using the names of the nodes
    [rgb, semantic],
    [rgb, depth],
    [rgb, depth, semantic]
]
voteFunction: simpleMean
prologue: pseudolabels_new_dataset_every_iteration
hyperParameters:
  semantic:
    semantic_classes: 12
    semantic_colors: [
      [0, 0, 0],
      [0, 0, 1],
      [0, 0, 2],
      [0, 0, 3],
      [0, 0, 4],
      [0, 0, 5],
      [0, 0, 6],
      [0, 0, 7],
      [0, 0, 8],
      [0, 0, 9],
      [0, 0, 10],
      [0, 0, 11],
    ]
    one_hot_encoding: True
  depth:
    max_depth_meters: 1
"""

graphV1ResidualCfgStr = """
NGC-Architecture: NGC-V1-Residual
nodeTypes: [RGB, HSV, Semantic, Depth]
nodeNames: [rgb, hsv, semantic, depth]
inputNodes: [rgb, hsv]
residualNode: rgb
edges: [
  [rgb, depth],
  [rgb, semantic],
  [hsv, depth],
  [rgb, semantic, depth],
  [hsv, depth, semantic]
]
voteFunction: simpleMean
prologue: pseudolabels_new_dataset_every_iteration
hyperParameters:
  semantic:
    semantic_classes: 12
    semantic_colors: [
      [0, 0, 0],
      [0, 0, 1],
      [0, 0, 2],
      [0, 0, 3],
      [0, 0, 4],
      [0, 0, 5],
      [0, 0, 6],
      [0, 0, 7],
      [0, 0, 8],
      [0, 0, 9],
      [0, 0, 10],
      [0, 0, 11],
    ]
    one_hot_encoding: True
  depth:
    max_depth_meters: 1
"""

trainCfgStr = """
seed: 42
numEpochs: 3
loader_params:
  batch_size: 10
optimizer:
  type: adamw
  args:
    lr: 0.01
scheduler:
  type: "ReduceLROnPlateau"
  args:
    mode: "min"
    factor: 0.9
    patience: 10
EarlyStopping:
  metric_name: Loss
  min_delta: 0
  patience: 10
  percentage: False
"""

def generateData():
    # Generate data
    global tmpDirName
    if tmpDirName is None:
        if __name__ == "__main__":
            tmpDirName = Path("/tmp/tmp8mddpuw9")
        else:
            tmpDirName = Path(tempfile.TemporaryDirectory().name)
    print(tmpDirName)
    if not (tmpDirName / "iter1/data").exists():
        generate_random_data(base_dir=tmpDirName / "iter1/data", representations=["rgb", "hsv", "depth", "semantic"], \
            dims=[3, 3, 1, 12], types=["float", "float", "float", "categorical"], shape=(240, 426), \
            N=80, prefix="train_")
    if not (tmpDirName / "validation").exists():
        generate_random_data(base_dir=tmpDirName / "validation", representations=["rgb", "hsv", "depth", "semantic"], \
            dims=[3, 3, 1, 12], types=["float", "float", "float", "categorical"], shape=(240, 426), N=20)
    if not (tmpDirName / "semisupervised").exists():
        generate_random_data(base_dir=tmpDirName / "semisupervised", representations=["rgb", "hsv"], dims=[3, 3], \
            types=["float", "float"], shape=(240, 426), N=100)
    return tmpDirName

def cleanup(base_dir: Path):
    shutil.rmtree(base_dir/"iter1/models") if (base_dir/"iter1/models").exists() else None
    shutil.rmtree(base_dir/"iter2") if (base_dir/"iter2").exists() else None
    shutil.rmtree(base_dir/"iter3") if (base_dir/"iter3").exists() else None

def get_nodes(cfg: Dict) -> List[Node]:
    """Basic implementation of getNodes, creating a subtype for all required types by adding a linear projection
    for the encoder and identity for decoder.
    """
    nodes = []
    for i in range(len(cfg["nodeNames"])):
        node_name = cfg["nodeNames"][i]
        node_params = {} if not node_name in cfg["hyperParameters"] else cfg["hyperParameters"][node_name]
        node_params["name"] = node_name
        node_type = cfg["nodeTypes"][i]
        node_type = globals()[node_type]
        node = build_dummy_linear_node(node_type)(**node_params)
        nodes.append(node)
    return nodes


def train(trainCfg, graphCfg):
    base_dir = generateData()
    cleanup(base_dir)
    ngcDir = NGCDir(base_dir, graphCfg)
    print(ngcDir)

    nodes = get_nodes(graphCfg)
    print(f"Nodes: {nodes}")

    model = build_model(nodes, graphCfg)
    print(LightningModuleEnhanced(model).summary)

    trainer = NGCTrainer(model=model, ngc_dir_path=base_dir, train_cfg=trainCfg,
                        train_dir=base_dir / "iter1/data", validation_dir=base_dir / "validation",
                        semisupervised_dirs=[base_dir / "semisupervised"], max_iterations=2)
    trainer.run(start_iteration=1, end_iteration=2, parallelization=False, debug=False)

    del model
    cleanup(base_dir)

class TestOneClick:
    def test_one_click_v1(self):
        trainCfg = yaml.safe_load(trainCfgStr)
        graphCfg = yaml.safe_load(graphV1CfgStr)
        train(trainCfg, graphCfg)

    def test_one_click_v2(self):
        trainCfg = yaml.safe_load(trainCfgStr)
        graphCfg = yaml.safe_load(graphV1ResidualCfgStr)
        train(trainCfg, graphCfg)

if __name__ == "__main__":
    # TestOneClick().test_one_click_v1()
    TestOneClick().test_one_click_v2()
