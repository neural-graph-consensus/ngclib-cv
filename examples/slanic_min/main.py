import yaml
import torch as tr
from functools import partial
from argparse import ArgumentParser
from pathlib import Path
from ngclib.models import build_model
from ngclib.trainer import NGCTrainer
from ngclib_cv.data import get_augmentation, nodes_plot_fn
from lightning_module_enhanced import LightningModuleEnhanced
from lightning_module_enhanced.callbacks import PlotCallback
from ngclib.utils import GraphCfg

device = tr.device("cuda") if tr.cuda.is_available() else tr.device("cpu")

def get_args():
    parser = ArgumentParser()
    parser.add_argument("--graph_cfg_path", required=True)
    parser.add_argument("--train_cfg_path", required=True)
    parser.add_argument("--dataset_path", required=True)
    parser.add_argument("--nodes_path", required=True)
    parser.add_argument("--ngc_dir", required=True)
    parser.add_argument("--parallelization", type=int, default=0)
    parser.add_argument("--debug", type=int, default=0)
    args = parser.parse_args()

    args.ngc_dir = Path(args.ngc_dir).absolute()
    args.dataset_path = Path(args.dataset_path).absolute()
    args.train_dir = args.dataset_path / "train"
    args.validation_dir = args.dataset_path / "validation"
    args.semisupervised_dir = args.dataset_path / "semisupervised"
    args.graph_cfg = yaml.safe_load(open(args.graph_cfg_path, "r"))
    args.train_cfg = yaml.safe_load(open(args.train_cfg_path, "r"))
    return args

def main():
    args = get_args()
    nodes = GraphCfg(args.graph_cfg).get_nodes_from_module(args.nodes_path)
    print(nodes)
    model = build_model(nodes, args.graph_cfg)
    print(LightningModuleEnhanced(model).summary)

    callbacks = [PlotCallback(partial(nodes_plot_fn, nodes=nodes))]
    trainer = NGCTrainer(model=model, ngc_dir_path=args.ngc_dir, train_cfg=args.train_cfg, train_dir=args.train_dir,
                        validation_dir=args.validation_dir, semisupervised_dirs=[args.semisupervised_dir],
                        edge_callbacks=callbacks, augmentation_fn=get_augmentation)
    trainer.run(start_iteration=1, end_iteration=3, parallelization=args.parallelization, debug=args.debug)

if __name__ == "__main__":
    main()
