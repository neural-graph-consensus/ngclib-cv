import yaml
import shutil
import os
import tempfile
import numpy as np
from typing import Dict, List
from pathlib import Path
from ngclib.utils import generate_random_data, NGCDir
from ngclib.trainer import NGCTrainer
from ngclib_cv.nodes import *
from ngclib.models import build_model
from nwgraph import Node
from nwgraph.node import build_dummy_linear_node
from lightning_module_enhanced import LightningModuleEnhanced

tmpDirName = None

graphV1CfgStr = """
NGC-Architecture: NGC-V1
nodeTypes: [RGB, RGB, RGB, RGB, Semantic]
nodeNames: [rgb1, rgb2, rgb3, rgb4, semantic]
inputNodes: [rgb1, rgb2, rgb3, rgb4]
edges: [ # Edges are defined using the names of the nodes
    [rgb1, semantic],
    [rgb2, semantic],
    [rgb3, semantic],
    [rgb4, semantic],
]
voteFunction: simpleMean
prologue: pseudolabels_new_dataset_every_iteration
hyperParameters:
  semantic:
    semantic_classes: 12
    semantic_colors: [
      [0, 0, 0],
      [0, 0, 1],
      [0, 0, 2],
      [0, 0, 3],
      [0, 0, 4],
      [0, 0, 5],
      [0, 0, 6],
      [0, 0, 7],
      [0, 0, 8],
      [0, 0, 9],
      [0, 0, 10],
      [0, 0, 11],
    ]
    one_hot_encoding: True
"""

trainCfgStr = """
seed: 42
cudnn_determnisim: True
batchSize: 10
numEpochs: 3
optimizer:
  type: adamw
  args:
    lr: 0.01
scheduler:
  type: "ReduceLROnPlateau"
  args:
    mode: "min"
    factor: 0.9
    patience: 10
"""

def generateData():
    # Generate data
    global tmpDirName
    if tmpDirName is None:
        if __name__ == "__main__":
            tmpDirName = Path("/tmp/tmp8mddpuw8")
        else:
            tmpDirName = Path(tempfile.TemporaryDirectory().name)
    print(tmpDirName)
    if not (tmpDirName / "iter1/data").exists():
        generate_random_data(base_dir=tmpDirName / "iter1/data", representations=["rgb1", "semantic"], \
            dims=[3, 12], types=["float", "categorical"], shape=(240, 426), N=20, prefix="train_")
    if not (tmpDirName / "validation").exists():
        generate_random_data(base_dir=tmpDirName / "validation", representations=["rgb1", "semantic"], \
            dims=[3, 12], types=["float", "categorical"], shape=(240, 426), N=5)
    if not (tmpDirName / "semisupervised").exists():
        generate_random_data(base_dir=tmpDirName / "semisupervised", representations=["rgb1"], dims=[3, 3], \
            types=["float"], shape=(240, 426), N=10)

    def f(a, b):
        if not (tmpDirName / b).exists():
            os.symlink(str(tmpDirName / a), str(tmpDirName / b))
    f((tmpDirName / "iter1/data/rgb1"), (tmpDirName / "iter1/data/rgb2"))
    f((tmpDirName / "iter1/data/rgb1"), (tmpDirName / "iter1/data/rgb3"))
    f((tmpDirName / "iter1/data/rgb1"), (tmpDirName / "iter1/data/rgb4"))
    f((tmpDirName / "validation/rgb1"), (tmpDirName / "validation/rgb2"))
    f((tmpDirName / "validation/rgb1"), (tmpDirName / "validation/rgb3"))
    f((tmpDirName / "validation/rgb1"), (tmpDirName / "validation/rgb4"))
    f((tmpDirName / "semisupervised/rgb1"), (tmpDirName / "semisupervised/rgb2"))
    f((tmpDirName / "semisupervised/rgb1"), (tmpDirName / "semisupervised/rgb3"))
    f((tmpDirName / "semisupervised/rgb1"), (tmpDirName / "semisupervised/rgb4"))
    return tmpDirName

def get_nodes(cfg: Dict) -> List[Node]:
    """Basic implementation of getNodes, creating a subtype for all required types by adding a linear projection
    for the encoder and identity for decoder.
    """
    nodes = []
    for i in range(len(cfg["nodeNames"])):
        node_name = cfg["nodeNames"][i]
        node_params = {} if not node_name in cfg["hyperParameters"] else cfg["hyperParameters"][node_name]
        node_params["name"] = node_name
        node_type = cfg["nodeTypes"][i]
        node_type = globals()[node_type]
        node = build_dummy_linear_node(node_type)(**node_params)
        nodes.append(node)
    return nodes

def cleanup(base_dir: Path):
    shutil.rmtree(base_dir/"iter1/models") if (base_dir/"iter1/models").exists() else None
    shutil.rmtree(base_dir/"iter2") if (base_dir/"iter2").exists() else None
    shutil.rmtree(base_dir/"iter3") if (base_dir/"iter3").exists() else None

def train(trainCfg, graphCfg):
    base_dir = generateData()
    cleanup(base_dir)
    ngcDir = NGCDir(base_dir, graphCfg)
    print(ngcDir)

    nodes = get_nodes(graphCfg)
    print(f"Nodes: {nodes}")

    model = build_model(nodes, graphCfg)
    print(LightningModuleEnhanced(model).summary)

    trainer = NGCTrainer(model=model, ngc_dir_path=base_dir, train_cfg=trainCfg,
                        train_dir=base_dir / "iter1/data", validation_dir=base_dir / "validation",
                        semisupervised_dirs=[base_dir / "semisupervised"])
    trainer.run(start_iteration=1, end_iteration=2, parallelization=False, debug=False)

    return model, base_dir

class TestReproductibility:
    def test_reproductibility_1(self):
        trainCfg = yaml.safe_load(trainCfgStr)
        graphCfg = yaml.safe_load(graphV1CfgStr)
        model, base_dir = train(trainCfg, graphCfg)

        new_data = np.random.randn(10, 240, 426, 3).astype(np.float32)
        model.load_all_edges(base_dir / "iter1/models")
        res = [edge.np_forward({edge.input_node: new_data}).numpy() for edge in model.edges]
        assert np.std(res, axis=0).sum() <= 1e-3
        cleanup(base_dir)

if __name__ == "__main__":
    TestReproductibility().test_reproductibility_1()
